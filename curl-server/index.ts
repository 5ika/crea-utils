import express from 'npm:express';
import morgan from 'npm:morgan';
import bodyParser from 'npm:body-parser';

const app = express();
const port = 8080;

app.use(morgan());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((req, res, next) => {
  res.set("ServerName", "CURL Server from 5ika. Question 'f' faite.");
  next();
});

app.get("/", (req, res) => {
  console.log(req.query);
  console.log();

  if (Object.keys(req.query)?.length > 0) {
    if (req.query.name)
      res.send(
        `Salut ${req.query.name}, j'ai bien reçu le paramètre GET. Ça joue pour la 'c' 💪\n`
      );
    else res.send("Mmh presque. Il faut nommer le paramètre 'name' 🤌\n");
  } else
    res.send("Okay, c'est bien une requête GET. Tout bon pour la 'a' 👌\n");
});

app.post("/", (req, res) => {
  if (req.body.getId && req.body.name) {
    if (req.body.getId === "12")
      res.send(`Salut ${req.body.name}, c'est tout bon pour la 'd' 👍\n`);
    else
      res.send(
        `Presque, c'est pas le bon getId. Il faut chercher le 12, pas le ${req.body.getId} 🦵\n`
      );
  } else if (Object.keys(req.query)?.length > 0)
    res.send(
      "Nope, il faut envoyer des paramètres POST, pas des paramètres GET 👀\n"
    );
  else res.send("Une requête POST, c'est nickel pour la 'b' 👍\n");
});

app.delete("/", (req, res) => {
  res.send("C'est bien une requête DELETE. Question 'e' validée 👌\n");
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
