## NGINX-SSH

Créé une image Docker avec NGINX et un serveur SSH. Il prend en paramètre les variables `USER` et `PASSWORD` pour configurer l'utilisateur SSH.

```bash
docker run -it -e USER=tim -e PASSWORD=yolo 5ika/nginx-ssh
```

## NGINX-w-Users

Basée sur NGINX-SSH, l'image rajoute:

- Une config custom pour NGINX qui utilise `/data` comme dossier à publier.
- Des scripts utiles:
  - `set-user`: Ajouter facilement un utilisateur qui peut se connecter par SSH et publier des fichiers avec NGINX dans un dossier personnel dans `/data`.
    `set-user tim yolo` 

```bash
docker run -it -e USER=tim -e PASSWORD=yolo 5ika/nginx-w-users
docker exec -it <container> set-user patate patate
```

